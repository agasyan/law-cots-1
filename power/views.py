from django.shortcuts import render

from rest_framework.response import Response

#for function based views
from rest_framework.decorators import api_view

#for class based views
from rest_framework.views import APIView

#for apiroot reverse
from rest_framework.reverse import reverse


@api_view()
def power(request):
    try:
        num = int(request.GET.get('num'))
        power = int(request.GET.get('power'))

        # For Store Calculation
        tmphasil = num ** power
        return Response({'function': 'Power. ' + 'angka: ' + str(num) + ' pangkat: ' + str(power) ,'result': tmphasil, 'status': 'success'})
    except Exception as e:
        return Response({'result': 'there was an error ' + str(e), 'status': 'error'})
