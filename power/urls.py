from django.urls import path
from django.conf.urls import include, url

from power import views


#url for app
urlpatterns = [
    #functions
    url(r'^api/power/?$', views.power, name='power'),
]